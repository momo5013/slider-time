# SliderTime

#### 介绍
时间范围滑块选择插件

基于 layui.slider 扩展的时间范围选择插件

#### 软件架构
layui js

#### Demo演示

[SliderTime Demo演示](https://www.itmm.wang/module/demo/sliderTime.html)

#### 安装教程

```
layui.config({
    base: ...
}).extend({
    sliderTime: "sliderTime/sliderTime"
});
```


#### 使用说明

1. sliderTime 基于 layui.slider，所以 slider 的配置都得以保留
1. range 参数强制为 true
1. 新增 disabledValue 属性，禁止选择时间范围   
   例如：[["10:00", "10:30"], ["14:00", "15:30"]]
1. 新增 disabledText 属性，禁止选择时间范围被选中后的提示内容，不配置不显示提示   
   例如："{start} - {end} 已占用"，支持start和end时间占位符
1. 新增 getValue 方法，返回当前选择的时间段（时间格式）
1. 新增 isOccupation 方法，返回当前选择的时间段是否包含了禁止选择的时间段
1. 新增 numToTime 公用方法，用于将十进制转为时间格式。例如：480 → 08:00
1. 新增 timeToNum 公用方法，用于将时间格式转为十进制。例如：24:00 → 1440


#### 示例
![输入图片说明](https://img.itmm.wang/image/blog/20210420163045.png "在这里输入图片标题")
#### 示例代码
```
layui.use(['sliderTime'], function () {
    var sliderTime = layui.sliderTime;

    var slider = sliderTime.render({
          elem: '#id'  //绑定元素
        , min: "08:00"
        , max: "18:00"
        , step: 10
        , disabledValue: [["10:00", "10:30"], ["14:00", "15:30"]] // 回显禁止选择时间段
        , disabledText: '{start} ~ {end} 已占用'
        , setTips: function (value) {
            //自定义提示文本
            return sliderTime.numToTime(value);
        }, change: function (value) {
            console.log(value[0] + "-" + value[1])
        }
    });
});
```
